CREATE DATABASE bdb_spotify;

USE bdb_spotify;

CREATE TABLE users(
    idUser VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30),
    email VARCHAR(30),
    dateAt VARCHAR(30)
);

CREATE TABLE artist(
    idArtist VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30)
);

CREATE TABLE albums(
    idAlbum VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30),
    artUrl VARCHAR(30),
    startDate VARCHAR(30),
    idArtist VARCHAR(30) REFERENCES artist (idArtist)
);

CREATE TABLE genres(
    idGenre VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30)
);

CREATE TABLE songs(
    idSong VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30),
    duration VARCHAR(30),
    idGenres VARCHAR(30) REFERENCES genres (idGenre),
    idArtist VARCHAR(30) REFERENCES artist (idArtist),
    idAlbum VARCHAR(30) REFERENCES albums (idAlbum)
);

CREATE TABLE playlists(
    idPlaylist VARCHAR(30) PRIMARY KEY,
    name VARCHAR(30),
    idUser VARCHAR(30) REFERENCES users(idUser)
);

CREATE TABLE songs_playlist(
    idSongplaylist VARCHAR(30) PRIMARY KEY,
    idPlaylist VARCHAR(30) REFERENCES playlists (idPlaylist),
    idSong VARCHAR(30) REFERENCES songs(idSong)
);

ALTER TABLE
    `users`
MODIFY
    `idUser` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `albums`
MODIFY
    `idAlbum` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `artist`
MODIFY
    `idArtist` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `genres`
MODIFY
    `idGenres` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `playlists`
MODIFY
    `idPlaylist` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `songs`
MODIFY
    `idSong` int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE
    `songs_playlist`
MODIFY
    `idSongplaylist` int(11) NOT NULL AUTO_INCREMENT;