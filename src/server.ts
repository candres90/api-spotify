import express, { Application, Request, Response } from "express";
import morgan from "morgan";
import compression from "compression";
import cors from "cors";

import usersRoutes from "./routes/users.routes";
import artistRoutes from "./routes/artist.routes"; 
import albumsRoutes from "./routes/albums.routes"; 
import genresRoutes from "./routes/genres.routes"; 


export class Server {
  private app: Application;

  constructor() {
    this.app = express();
    this.settings();
    this.middlewares();
    this.routes();
  }

  settings() {
    this.app.set("port", process.env.PORT || 8080);
  }

  middlewares() {
    this.app.use(morgan("dev"));
    this.app.use(express.json());
    this.app.use(cors());
    this.app.use(compression());
  }

  routes() {
    this.app.use('/api/users/',usersRoutes);
    this.app.use('/api/artist/',artistRoutes);
    this.app.use('/api/albums/',albumsRoutes);
    this.app.use('/api/genres/',genresRoutes);
    
    
  }

  async listen(){
      await this.app.listen(this.app.get('port'));
      console.log(`Esta corriendo por el puerto ${this.app.get('port')}`)
  }

}
