import { Response, Request } from 'express';
import { connect } from '../config/database';

export async function getSongsPlaylist(req:Request, res: Response): Promise<Response | void> {
    try {
       const conn = await connect();
       const dataUsers = await conn.query('SELECT * FROM songs_playlist');
       return res.json(dataUsers[0]);
    } catch (error) {
       console.log(error);
        return res.json({code: 300, info: 'error al buscar registros'});
    }
 
 }


 export async function postSongsPlaylist(req:Request, res: Response): Promise<Response | void> {
 
    let PlaylistPost = {
        idPlaylist: req.body.idPlaylist,
        idSong: req.body.idSong
    }

    const conn = await connect();
    try {
       console.log(req.body)
      let ramdomNumber = Math.random();
      await conn.query('INSERT INTO songs_playlist value (?, ?)',[ramdomNumber, PlaylistPost.idPlaylist, PlaylistPost.idSong] );
      return res.json({code: 200, info: 'Nuevo registro ingresado'});
    } catch (error) {
       console.log(error);
       return res.json({code: 300, info: 'error al ingresar registro'});
    }

 }

 export async function deletesongs_playlist(req:Request, res: Response): Promise<Response | void> {
    try {
       const conn = await connect();
       const resp  =  await conn.query('DELETE FROM songs_playlist WHERE idPlaylist = ?', req.params.id);
       return res.json(resp[0]);
    } catch (error) {
       console.log(error);
       return res.json({code: 300, info: 'error al eliminar registro'});
    }

}
