import { Response, Request } from 'express';
import { connect } from '../config/database';


export async function getUser(req:Request, res: Response): Promise<Response | void> {
   try {
      const conn = await connect();
      const dataUsers = await conn.query('SELECT * FROM users');
      return res.json(dataUsers[0]);
   } catch (error) {
      console.log(error);
       return res.json({code: 300, info: 'error al buscar registros'});
   }

}


export async function postUser(req:Request, res: Response): Promise<Response | void> {

   let userPost = {
       name: req.body.name,
       email: req.body.email,
       dateAt: new Date().toISOString().slice(0, 10)
   }

   const conn = await connect();
   try {
     await conn.query('INSERT INTO users value ( ?, ?, ?, ?)',[0, userPost.name, userPost.email, userPost.dateAt] );
     return res.json({code: 200, info: 'Nuevo usuario registrado'});
   } catch (error) {
      console.log(error);
      return res.json({code: 300, info: 'error al ingresar nuevo artista'});
   }

}


export async function putUser(req:Request, res: Response): Promise<Response | void> {

   let usersPut = {
       name: req.body.name,
       email: req.body.email
   }

   try {
     const conn = await connect();
     await conn.query(`UPDATE users SET name = '${usersPut.name}', email = '${usersPut.email}' WHERE idUser = ${req.params.id}`,  ); 
     return res.json({code: 200, info: 'Registro actualizado'});
   } catch (error) {
     console.log(error);
     return res.json({code: 300, info: 'error al actualizar registro de artista'});
   }

}


export async function deleteUser(req:Request, res: Response): Promise<Response | void> {
   try {
      const conn = await connect();
      const resp  =  await conn.query('DELETE FROM users WHERE idUser = ?', req.params.id);
      return res.json(resp[0]);
   } catch (error) {
      console.log(error);
      return res.json({code: 300, info: 'error al eliminar registro'});
   }

}


