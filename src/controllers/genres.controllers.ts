import {Response, Request} from 'express';
import { connect } from '../config/database';

export async function getgenres(req: Request, res: Response): Promise<Response | void>{
    try {
    const conn = await connect();
    const datagenres = await conn.query('SELECT * FROM genres');
    return res.json(datagenres[0]);
     } catch (error) {
        console.log(error);
         return res.json({code: 300, info: 'error al buscar generos'});
     }
}

export async function getIdgenres(req: Request, res: Response): Promise<Response | void>{

    try {
        const {id} = req.params;
        const conn = await connect();
        const datagenres = await conn.query('SELECT * FROM genres WHERE idgenre = ' + id);
        return res.json(datagenres[0]);
         } catch (error) {
            console.log(error);
             return res.json({code: 300, info: 'error al buscar generos'});
         }

}

export async function postgenres(req:Request, res:Response): Promise<Response | void>{

    try {
        let genres = {
            name: req.body.name
        }
        const conn = await connect();
        await conn.query('INSERT INTO genres SET ?', genres);
        return res.json({code: 200, info: 'Genero guardado!'});   
    } catch (error) {
        console.log(error);
        return res.json({code: 300, info: 'error al guardando genero'});
    }
   
}

export async function putgenres(req:Request, res:Response): Promise<Response | void>{
    try {
        const conn = await connect();
        await conn.query("UPDATE genres SET name = '" + req.body.name + "' WHERE idgenre = " + req.body.idgenre);
        return res.json({code: 200, info: 'El genero '+ req.body.idgenre +' se ha actualizado!'}); 
    } catch (error) {
       
        return res.json({code: 300, info: 'Error actualizando el genero'});
    }
  
}

export async function deletegenres(req:Request, res:Response): Promise<Response | void>{

    try {
        const {id} = req.params;

        const conn = await connect();
        await conn.query('DELETE FROM genres WHERE idgenre = ' + id);
        return res.json({code: 200, info: 'Genero '+ id +' Eliminado!'});
    } catch (error) {
       
        return res.json({code: 300, info: 'No se pudo eliminar el genero'});
    }
   
}