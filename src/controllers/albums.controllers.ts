import { Response, Request } from 'express';
import { connect } from '../config/database';

export async function getAlbum(req:Request, res: Response): Promise<Response | void> {
    try {
       const conn = await connect();
       const dataUsers = await conn.query('SELECT * FROM albums');
       return res.json(dataUsers[0]);
    } catch (error) {
       console.log(error);
        return res.json({code: 300, info: 'error al buscar registros'});
    }
 
 }
 
 
 export async function postAlbum(req:Request, res: Response): Promise<Response | void> {
 
    let userPost = {
        name: req.body.name,
        email: req.body.email,
        dateAt: req.body.dateAt
    }
 
    const conn = await connect();
    try {
       console.log(req.body)
      let ramdomNumber = Math.random();
      //await conn.query('INSERT INTO artist value (?, ?)',[ramdomNumber, artistPost.name] );
      await conn.query('INSERT INTO albums value (?, ?, ?, ?)',[ramdomNumber, userPost.name, userPost.email, userPost.dateAt] );
      return res.json({code: 200, info: 'Nuevo usuario registrado'});
    } catch (error) {
       console.log(error);
       return res.json({code: 300, info: 'error al ingresar nuevo albums'});
    }
 
 }



 export async function putAlbums(req:Request, res: Response): Promise<Response | void> {
 
    let albumPut = {
        name: req.body.name,
        artUrl: req.body.artUrl
    }

    try {
      const conn = await connect();
      await conn.query(`UPDATE albums SET name = '${albumPut.name}', artUrl = '${albumPut.artUrl}' WHERE idAlbum =${req.params.id}`); 
      return res.json({code: 200, info: 'album actualizado'});
    } catch (error) {
      console.log(error);
      return res.json({code: 300, info: 'error al actualizar registro de album'});
    }

 }
 
  export async function deleteAlbums(req:Request, res: Response): Promise<Response | void> {
       try {
          const conn = await connect();
          const resp  =  await conn.query('DELETE FROM albums WHERE idAlbum = ?', req.params.id);
          return res.json(resp[0]);
       } catch (error) {
          console.log(error);
          return res.json({code: 300, info: 'error al eliminar registro de albums'});
       }
 
  }