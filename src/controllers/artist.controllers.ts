import { Response, Request } from 'express';
import { connect } from '../config/database';

export async function getArtists(req:Request, res: Response): Promise<Response | void> {
    try {
       const conn = await connect();
       const dataUsers = await conn.query('SELECT * FROM artist');
       return res.json(dataUsers[0]);
    } catch (error) {
       console.log(error);
        return res.json({code: 300, info: 'error al buscar registros'});
    }
 
 }
 
 
 export async function postArtist(req:Request, res: Response): Promise<Response | void> {
 
     let artistPost = {
         name: req.body.name
     }
 
     const conn = await connect();
     try {
        console.log(req.body)
       let ramdomNumber = Math.random();
       await conn.query('INSERT INTO artist value (?, ?)',[ramdomNumber, artistPost.name] );
       return res.json({code: 200, info: 'Nuevo artista registrado'});
     } catch (error) {
        console.log(error);
        return res.json({code: 300, info: 'error al ingresar nuevo artista'});
     }
 
  }

   
  export async function putArtist(req:Request, res: Response): Promise<Response | void> {
 
    let artistPut = {
      idArtist: req.body.idArtist,
        name: req.body.name
    }

    try {
      const conn = await connect();
      await conn.query(`UPDATE artist SET idArtist = ? WHERE idArtist = '${req.body.idArtist}'`, req.params.id ); 
      return res.json({code: 200, info: 'Artista actualizado'});
    } catch (error) {
      console.log(error);
      return res.json({code: 300, info: 'error al actualizar registro de artista'});
    }

 }
 
  export async function deleteArtist(req:Request, res: Response): Promise<Response | void> {
       try {
          const conn = await connect();
          const resp  =  await conn.query('DELETE FROM artist WHERE idArtist = ?', req.params.id);
          return res.json(resp[0]);
       } catch (error) {
          console.log(error);
          return res.json({code: 300, info: 'error al eliminar registro de artista'});
       }
 
  }
 
 