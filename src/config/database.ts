import { createPool, Pool } from 'mysql2/promise';

export async function connect(): Promise<Pool> {
    
    const connection = await createPool({
        host: 'localhost',
        database: 'bdb_spotify',
        user: 'root',
        password: '',
        connectionLimit: 10
    });

    return connection;
}