import { Router } from "express";

const usersRoutes = Router();

import { postUser, putUser, getUser, deleteUser } from "../controllers/users.controllers";

usersRoutes.route('/').get(getUser);
usersRoutes.route('/').post(postUser);
usersRoutes.route('/:id').put(putUser);
usersRoutes.route('/:id').delete(deleteUser);

export default usersRoutes;