import { Router } from "express";

const artistRoutes = Router();

import {
  postArtist,
  getArtists,
  deleteArtist,
  putArtist,
} from "../controllers/artist.controllers";

artistRoutes.route("/").get(getArtists);
artistRoutes.route("/").post(postArtist);
artistRoutes.route("/:id").put(putArtist);
artistRoutes.route("/:id").delete(deleteArtist);

export default artistRoutes;
