import { Router } from "express";

const albumsRoutes = Router();

import {
getAlbum,
postAlbum
} from "../controllers/albums.controllers";

albumsRoutes.route("/").get(getAlbum);
albumsRoutes.route("/").post(postAlbum);
// artistRoutes.route("/:id").put(putArtist);
// artistRoutes.route("/:id").delete(deleteArtist);

export default albumsRoutes;
