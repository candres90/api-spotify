import {Router} from 'express';

const genresRoutes = Router();

import {deletegenres, getIdgenres, getgenres, postgenres, putgenres} from '../controllers/genres.controllers';

genresRoutes.route('/').get(getgenres);
genresRoutes.route('/:id').get(getIdgenres);
genresRoutes.route('/').post(postgenres);
genresRoutes.route('/:id').delete(deletegenres);
genresRoutes.route('/').put(putgenres);

export default genresRoutes;